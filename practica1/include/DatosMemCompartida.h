// Esfera.h: interface for the Esfera class.
//
//////////////////////////////////////////////////////////////////////


#if !defined(AFX_DATOSMEMCOMPARTIDA_H__D5740FCC_9B1B_48DD_8642_77B83D54C6A2__INCLUDED_)
#define AFX_DATOSMEMCOMPARTIDA_H__D5740FCC_9B1B_48DD_8642_77B83D54C6A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
 
#include "Esfera.h"
#include "Raqueta.h"

class DatosMemCompartida
{       
public:         
      Esfera esfera;
      Raqueta raqueta1;
      int accion; //1 arriba, 0 nada, -1 abajo
};

#endif
